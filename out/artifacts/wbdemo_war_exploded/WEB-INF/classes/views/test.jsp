<%--
  Created by IntelliJ IDEA.
  User: 86158
  Date: 2022/4/24
  Time: 18:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>查找</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">

    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="js/html5shiv.js"></script>-->
    <!--    <script src="js/respond.min.js"></script>-->
    <!--[endif]-->
</head>

<body>
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->
        <div class="row" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="find">查找博客</a></li>
                <li ><a href="findPerson">查找用户</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
        <div class="row" style="margin-left: 40px">
            <div class="col-md-10" >
                <div class="panel">
                    <div class="panel-body">
                        <div class="dir-info">
                            <div class="row">
                                <div class="col-xs-1">
                                    <div class="avatar">
                                        <img src="images/photos/user2.png" alt=""/>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <a class="username">
                                    </a>

                                    <span>个人简介：</span>

                                    <span class="introduction">

                                    </span><br>
                                    <span class="followered">

                                    </span>
                                </div>

                                <div class="col-xs-1">
                                    <div id="app"><br>
                                        <button id='state' v-on:click="btnClick"  v-bind:class="my_cls">{{ msg }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <input type="button" id="btn11" value="请求数据">
                <input type="button" id="clear" value="清除数据">
                <ul class="list"></ul>
            </div>

        </div>



    </div>
    <!-- main content end-->
</section>




<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">
    $(function(){
        $("#btn11").click(function(){
            $.ajax({
                type:"get",
                dataType:"json",
                url:"/person",
//						data:{},
                success:function(res){
                    $(".alert").html("请求成功")
                    var html='';
                    //第一次方式
//							$.each(res,function(i,v){
//								html+="<li data-id="+v.id+">"+v.name+"</li>"
//							})
                    //第二次方式
                    for(var i=0;i<res.length;i++){
                        console.log(res[i]);
                        //获取头像
                        html+="<div class="+"row"+"> <div class="+"col-xs-1"+
                            "> <div class="+'"avatar"'+"> <img src="+"\"picture\/"+ res[i].picture+"\""+" alt="+"\""+"\""+"/>";
                        //获取用户名
                        html+="</div> </div> <div class="+"col-xs-9"+"> <a ><h4 href="+"personBlog"+">"+res[i].userName;
                        //获取简介
                        html+="</h4></a> <span>"+"个人简介：</span> <span>"+res[i].introduction+"</span>";
                        //获取粉丝
                        html+="<br> <span>"+res[i].followed+"位关注者</span> </div> <div class="+"col-xs-1"+"> <div id="+'"app"';
                        html+= "><br> <button id="+"\'"+"state"+""+"\'" +"class="+"\"";
                        html+=res[i].status+"\""+">"+res[i].message+"</button>  </div></div></div></div>"

                    }
                    $(".dir-info").html(html)

                },
                error:function(){
                    $(".alert").html("请求失败")
                }
            })
        })
    })
</script>
<script type="text/javascript">
    $(function(){

        $("#state").click(function(){alert("按到了")
            $.ajax({
                type:"get",
                dataType:"json",
                url:"/update",
//						data:{},
                success:function(res){
                    $(".alert").html("请求成功")
                    alert("按到了")
                    var html='';
                    //第一次方式
//							$.each(res,function(i,v){
//								html+="<li data-id="+v.id+">"+v.name+"</li>"
//							})
                    //第二次方式
                    // for(var i=0;i<res.length;i++){
                    //     console.log(res[i]);
                    //     //获取头像
                    //     html+="<div class="+"row"+"> <div class="+"col-xs-1"+
                    //         "> <div class="+'"avatar"'+"> <img src="+"\"picture\/"+ res[i].picture+"\""+" alt="+"\""+"\""+"/>";
                    //     //获取用户名
                    //     html+="</div> </div> <div class="+"col-xs-9"+"> <a ><h4 href="+"personBlog"+">"+res[i].userName;
                    //     //获取简介
                    //     html+="</h4></a> <span>"+"个人简介：</span> <span>"+res[i].introduction+"</span>";
                    //     //获取粉丝
                    //     html+="<br> <span>"+res[i].followed+"位关注者</span> </div> <div class="+"col-xs-1"+"> <div id="+'"app"';
                    //     html+= "><br> <button id="+"\'"+"state"+""+"\'" +"class="+"\"";
                    //     html+=res[i].status+"\""+">"+res[i].message+"</button>  </div></div></div></div>"
                    //
                    // }
                    // $(".dir-info").html(html)

                },
                error:function(){
                    $(".alert").html("请求失败")
                }
            })
        })
    })
</script>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
</script>
<script>

    function toChange()
    {
        window.open("/find")
    }
</script>
<script src="js/vue.js"></script>
<script>
    new Vue({
        el:'#app',
        data:{
            msg:'关注',
            my_cls:'btn btn-success btn-sm'
        },
        methods:{
            btnClick:function(){
                if(this.my_cls=='btn btn-success btn-sm'){
                    this.my_cls='btn btn-default btn-sm';
                    this.msg='取消关注';
                    alert("关注成功！");
                }else{
                    this.my_cls='btn btn-success btn-sm';
                    this.msg='关注';
                    alert("取消关注成功！");
                }
            }
        }
    })
</script>
<script src="js/vue.js"></script>

<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>