<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>图片博客平台</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">

    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]-->
    <script src="js/vue.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>


    <![endif]-->
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <form role="form" action="poBlog" method="post" enctype="multipart/form-data">
                        <textarea name="blogArticle"  class="form-control input-lg p-text-area" rows="2" placeholder="分享新鲜事"></textarea>
                        <input type="hidden" name ="user_id" value="2">
                        <footer class="panel-footer">
                            <button class="btn btn-post pull-right" type="submit" onclick="publish()">发表</button>
                            <ul class="nav nav-pills p-option">
                                <li>
                                    <a href="#" onclick="findPeople()"><i class="fa fa-user"></i></a>
                                </li>
                                <li>
                                    <a href="#" onclick="addImage()"><i class="fa fa-camera" ></i></a>
                                    <input type="file" id="image" name="blogImage" style="display:none;"/>
                                </li>
                                <li>
                                    <a href="#" onclick="getAddress()"><i class="fa  fa-location-arrow"></i></a>
                                </li>

                            </ul>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11" style="margin-left: 20px;">
                <div class="panel">
                    <header class="panel-heading">
                        最新动态
                        <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                     </span>
                    </header>
                    <div class="panel-body" id="app">


                        <ul class="activity-list" v-for="a in items">

                            <br>
                            <div class="avatar">
                                <img :src="a.photo" alt=""/>
                            </div>
                            <div class="activity-desk">
                                <h4><a :href="'personblog?user_id='+a.user_id">{{a.user_name}}</a> </h4>
                                <p href="context"><h4>{{a.article}}</h4>   </p>

                                <div class="album" v-for="(p,id) in a.pic">
                                    <a href="#">
                                        <img :src="p.pic_addr">
                                    </a>


                                </div>

                                <div class="panel">

                                    <div class="panel-body">

                                    </div>
                                    <div class="posttime"></div>

                                    <div>
                                        <ul class="post-view" style="margin-top: 20px">
                                            <li >
                                                <a  :href="'context?user_id='+a.user_id+'&blog_id='+a.blog_id">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <span id="read" >0</span>
                                            </li>
                                            <li >
                                                <a  :href="'context?user_id='+a.user_id+'&blog_id='+a.blog_id">
                                                    <i class="fa fa-comment"></i>
                                                </a>
                                                <span id="comment" >{{a.commentsum}}</span>
                                            </li>
                                            <li>
                                                <a >
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                                <span id="love" >{{a.likesum}}</span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            </li>

                        </ul>

                    </div>

                </div>
            </div>

        </div>
        <div style="overflow: hidden;position: fixed;right: 10px;bottom: 20px;z-index: 10;">
            <div style="overflow: hidden;">
                <div style="padding-top:20px;padding-right:50px;padding-bottom:50px">
                    <a href="#" style="float: right;border-radius:50%;" class="btn btn-post pull-right btn-sm ">
                        <i class="fa fa-arrow-up" style="color: #FFFFFF"></i>
                    </a>
                </div>
            </div>
        </div>

        <!--body wrapper start-->

        <!--body wrapper end-->


    </div>
    <!-- main content end-->
</section>
<script src="js/vue.js"></script>

<script>

    var vm = new Vue({
        el:'#app',
        data:{
            items:'',
        },
        created:function(){
            var that = this;
            $.ajax({
                url:"/getBlogs",
                contentType:"application/json;charset=UTF-8",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    that.items = res;
                }
            })
        }
    })

</script>
<script src="js/jquery.nicescroll.js"></script>
<script type="text/javascript">
    $(document).ready(
        function() {
            $("html").niceScroll();
        }
    );
</script>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
</script>


<script>

    function toChange()
    {
        var key1=$("#key1").val();
        window.open("/findPerson?key1="+key1)
    }
</script>


<script>
    function addImage(){
        document.getElementById("image").click();
        document.getElementById("image").click();
    }
    function getAddress()
    {
        window.open ("location", "定位地址", "height=300, width=400, top=150, left=500, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no");
    }
    function publish()
    {
        alert("发表成功!");
    }
    function findPeople()
    {
        window.open ("littlePage", "选择要提到的好友", "height=300, width=900, top=150, left=500, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no");
    }
    function addLove()
    {

    }
</script>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>


<!--easy pie chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<script src="js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.time.js"></script>
<script src="js/main-chart.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>


</body>
</html>
