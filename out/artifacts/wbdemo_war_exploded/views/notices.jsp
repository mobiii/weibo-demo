<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>通知界面</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">

    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->

        <div class="row">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="mail-box">
                        <section class="mail-box-info">
                            <header class="header">
                                <div class="btn-group pull-right">
                                    <button class="btn btn-sm btn-primary" type="button">
                                        <i class="fa fa-star" style="color: #FFFFFF"></i>
                                    </button>
                                    <button class="btn btn-sm btn-primary" type="button">
                                        <i class="fa fa-bitbucket"></i>
                                    </button>
                                </div>
                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i></button>
                                    </div>
                                    <div class="btn-group select">
                                        <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">
                                            <span style="width: 70px;" class="dropdown-label">筛选</span>
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu text-left text-sm">
                                            <li><a href="#">已读</a></li>
                                            <li><a href="#">未读</a></li>
                                            <li><a href="#">星标</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </header>

                            <section class="mail-list">
                                <ul class="list-group ">
                                    <li class="list-group-item">
                                <span class="pull-left chk">
                                    <input type="checkbox"/>
                                </span>
                                        <a class="thumb pull-left" href="#"> <img src="images/avatar-mini.jpg"> </a>
                                        <a class="" href="#"> <small class="pull-right text-muted">1天前</small> <strong>John Doe</strong>  <span>在图片博客中提到了你</span> <span class="label label-sm btn-success">已读</span> </a>
                                    </li>
                                    <li class="list-group-item">
                                <span class="pull-left chk">
                                    <input type="checkbox"/>
                                </span>
                                        <a class="thumb pull-left" href="#"> <img src="images/photos/user1.png"> </a>
                                        <a class="" href="#"> <small class="pull-right text-muted">1天前</small> <strong>Jane Doe</strong> <span>在图片博客中提到了你</span> <span class="label label-sm btn-danger">未读</span> </a>
                                    </li>
                                    <li class="list-group-item">
                                <span class="pull-left chk">
                                    <input type="checkbox"/>
                                </span>
                                        <a class="thumb pull-left" href="#"> <img src="images/photos/user2.png"> </a>
                                        <a class="" href="#"> <small class="pull-right text-muted">3天前</small> <strong>Jonathan Smith</strong>  <span>评论了你</span> <span class="label label-sm btn-warning">星标</span> </a>
                                    </li>
                                    <li class="list-group-item">
                                <span class="pull-left chk">
                                    <input type="checkbox"/>
                                </span>
                                        <a class="thumb pull-left" href="#"> <img src="images/photos/user3.png"> </a>
                                        <a class="" href="#"> <small class="pull-right text-muted">4天前</small> <strong>John Doe</strong>  <span>评论了你</span> <span class="label label-sm btn-success">已读</span>  </a>
                                    </li>
                                    <li class="list-group-item">${message}</li>
                                    <li>我其实有在输出</li>
                                </ul>
                            </section>


                        </section>
                    </div>

                </div>

            </div>

        </div>
        <!--body wrapper start-->

        <!--body wrapper end-->

        <!--footer section start-->

        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
</script>
<script>

    function toChange()
    {
        window.open("/find")
    }
</script>
<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--easy pie chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<script src="js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.time.js"></script>
<script src="js/main-chart.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>


</body>
</html>
