<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>图片博客平台</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.min.js"></script>
    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->

        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body" id="con">
                        <ul class="activity-list">
                            <div class="avatar">
                                <img :src="blog.photo" alt=""/>
                            </div>
                            <div class="activity-desk">
                                <h4><a :href="'personblog?user_id='+blog.user_id">{{blog.user_name}}</a> </h4>
                                <p><h4>{{blog.article}}</h4>   </p>

                                <div class="album" v-for="(p,id) in blog.pic">
                                    <a href="#">
                                        <img :src="p.pic_addr">
                                    </a>
                                </div>
                            </div>
                        </ul>

                        <ul class="post-view">
                            <li>
                                <a  href="#">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <span id="read" >0</span>
                            </li>
                            <li >

                                <a  href="#">
                                    <i class="fa fa-comment"></i>
                                </a>
                                <span id="comment" >{{blog.commentsum}}</span>
                            </li>
                            <li  v-on:click="addlike(blog)">
                                <form id="likesform">
                                    <input type="hidden" name ="user_id" value=${user_id}>
                                    <input type="hidden" name ="blog_id" value=${blog_id}>
                                </form>
                                <a >
                                    <i class="fa fa-heart"></i>
                                </a>
                                <span id="love" >{{blog.likesum}}</span>
                            </li>
                        </ul>
                    </div>
                    <div>
                        <section class="mail-box-info" id="getcmt">
                            <section class="mail-list">
                                <div class="chat-form ">
                                    <form role="form" class="form-inline" id="commentform">
                                        <div class="form-group" >
                                            <input type="text" name="comment" id="cmtmessage" style="width: 100%" placeholder="Type a message here..." class="form-control">
                                            <input type="hidden" name ="user_id" value=${user_id}>
                                            <input type="hidden" name ="blog_id" value=${blog_id}>
                                        </div>
                                        <button class="btn btn-success" type="submit" id="addcomment">评论</button>
                                    </form>
                                </div>
                                <ul class="list-group "  v-for="a in items">
                                    <li class="list-group-item">
                                        <a class="thumb pull-left" href="#"> <img :src="a.photo"> </a>
                                        <a class="" href="#">  <strong>{{a.user_name}}</strong>  <span>{{a.message}}</span>  </a>
                                    </li>

                                </ul>
                            </section>


                        </section>

                    </div>

                </div>
            </div>

        </div>

    </div>
    <!-- main content end-->
</section>
<script src="js/vue.js"></script>
<script>

    var vm = new Vue({
        el:'#con',
        data:{
            blog:'',
        },
        created:function(){
            var that = this;
            $.ajax({
                url:"/getContext?blog_id="+${blog_id},
                contentType:"application/json;charset=UTF-8",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    that.blog = res;
                }
            })
        },
        methods: {
            addlike: function(blog) {
                var that = this;
                alert("点赞");
                var x=$("#likesform").serializeArray();
                $.ajax({
                    type:"post",
                    dataType:"json",
                    data:x,
                    url:"/addLike",
                    success:function(data){
                    }
                });
                $.ajax({
                    url:"/getContext?blog_id="+${blog_id},
                    contentType:"application/json;charset=UTF-8",
                    dataType:"json",
                    success:function(res){
                        console.log(res);
                        that.blog = res;
                    }
                })
            }
        }
    })

</script>

<script>

    var vm = new Vue({
        el:'#getcmt',
        data:{
            items:'',
        },
        created:function(){
            var that = this;
            $.ajax({
                url:"/getCmt?blog_id="+${blog_id},
                contentType:"application/json;charset=UTF-8",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    that.items = res;
                },
                error:function (e){
                    alert("fail");
                }
            })
        }
    })

</script>
<script>
    $("#addcomment").click(function(){

        var x=$("#commentform").serializeArray();


        if(""==$('#cmtmessage').val()){
            alert('评论内容不能为空！');
        }else{
            $.ajax({
                type:"post",
                dataType:"json",
                data:x,
                url:"/addCmt",
                success:function(data){
                    alert("评论成功");
                }
            });
        }

    });
</script>
<script>
    $("#addlike111").click(function(){
        alert("点赞");
        var x=$("#likesform").serializeArray();
        $.ajax({
            type:"post",
            dataType:"json",
            data:x,
            url:"/addLike",
            success:function(data){
                alert("点赞成功，点赞总数");
                var newData = JSON.stringify(data);    //将json对象转换为字符串
                newData = eval("("+newData+")");
                var likesum = newData.likesum;
                document.getElementById("love").innerText=likesum;
            }
        });


    });
</script>
<script>

    new Vue({
        el:'#app1',
        data: {
            read:0,
            comment:0,
            love:0
        },
        methods:{
            add:function (){
                this.read=this.read+1;
            },
            addLove:function (){
                this.love=this.love+1;
            }
        }
    })

</script>
<script>
    function addImage(){
        document.getElementById("image").click();
    }
    function getAddress()
    {
        window.open ("location", "定位地址", "height=300, width=400, top=150, left=500, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no");
    }
    function publish()
    {
        alert("发表成功!");
    }
    function findPeople()
    {
        window.open ("littlePage", "选择要提到的好友", "height=300, width=400, top=150, left=500, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no");
    }
</script>

<script>

    function toChange()
    {
        var key1=$("#key1").val();
        window.open("/findPerson?key1="+key1)
    }
</script>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
</script>
<script>

    function toChange()
    {
        window.open("/find")
    }
</script>
<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--easy pie chart-->
<script src="js/easypiechart/jquery.easypiechart.js"></script>
<script src="js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="js/sparkline/jquery.sparkline.js"></script>
<script src="js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="js/flot-chart/jquery.flot.js"></script>
<script src="js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="js/flot-chart/jquery.flot.resize.js"></script>
<script src="js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="js/flot-chart/jquery.flot.selection.js"></script>
<script src="js/flot-chart/jquery.flot.stack.js"></script>
<script src="js/flot-chart/jquery.flot.time.js"></script>
<script src="js/main-chart.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>


</body>
</html>
