<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>查找</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">

    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="js/html5shiv.js"></script>-->
    <!--    <script src="js/respond.min.js"></script>-->
    <![endif]-->
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>

        <!-- header section end-->
        <div class="row">
            <div style="margin-left:300px" >
                <button class="btn btn-default btn-s" onclick="window.location.href='find.html'"><i class="fa fa-search"></i> 查找博客</button>
                <button class="btn btn-default btn-s" onclick="window.location.href='findPerson.html'"><i class="fa fa-search"></i> 查找用户</button>
            </div>
        </div>
        <!-- /.navbar-collapse -->
        <div class="row">
            <div class="col-md-11" style="margin-left: 20px">
                <div class="panel">
                    <header class="panel-heading">
                        <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                     </span>
                    </header>
                    <div class="panel-body">
                        <ul class="activity-list">
                            <br>
                            <div class="avatar">
                                <img src="images/photos/user1.png" alt=""/>
                            </div>
                            <div class="activity-desk">
                                <h4><a href="personBlog">${message}</a> </h4>
                                <p href="context"><h4>春天来了，华农的花开了。</h4>   </p>

                                <div class="album">
                                    <a href="#">
                                        <img alt="" src="images/gallery/image1.jpg">
                                    </a>
                                    <a href="#">
                                        <img alt="" src="images/gallery/image2.jpg">
                                    </a>
                                    <a href="#">
                                        <img alt="" src="images/gallery/image3.jpg">
                                    </a>
                                </div>

                                <div class="panel">

                                    <div class="panel-body">

                                    </div>
                                    <div class="posttime">7分钟前，来自广东省广州市</div>

                                    <div id="app1">
                                        <ul class="post-view">
                                            <li>
                                                <a  href="context">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <span id="read" >{{read}}</span>
                                            </li>
                                            <li v-on:click="add">
                                                <a  href="context" >
                                                    <i class="fa fa-comment"></i>
                                                </a>
                                                <span id="comment" >{{comment}}</span>
                                            </li>
                                            <li v-on:click="addLove">
                                                <a >
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                                <span id="love" >{{love}}</span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            </li>
                            <li>
                                <div class="avatar">
                                    <img src="images/photos/user2.png" alt=""/>
                                </div>
                                <div class="activity-desk">
                                    <h4><a href="#">咩咩找咩</a> </h4>
                                    <div class="wbcomment">无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。无聊无聊好无聊。</div>
                                    <div class="text-muted">2分钟前，来自广东省广州市</div>
                                </div>
                            </li>

                            <li>
                                <div class="avatar">
                                    <img src="images/photos/user3.png" alt=""/>
                                </div>
                                <div class="activity-desk">

                                    <h5><a href="#">张家二丫</a></h5>
                                    <p>2022年的第一场雪，打卡成功！</p>
                                    <p class="text-muted">2天前，来自北京市朝阳区</p>
                                </div>
                            </li>

                            <li>
                                <div class="avatar">
                                    <img src="images/photos/user4.png" alt=""/>
                                </div>
                                <div class="activity-desk">

                                    <h5><a href="#">麻木不人</a> </h5>
                                    <span>今天和</span>
                                    <a href="personBlog" text="">@*00</a>
                                    <span>一起出来玩！</span>
                                    <p class="text-muted">4天前，来自广东省广州市</p>
                                </div>
                            </li>

                            <li>
                                <div class="avatar">
                                    <img src="images/photos/user5.png" alt=""/>
                                </div>
                                <div class="activity-desk">

                                    <h5><a href="#">美味的甜甜花酿鸡</a> </h5>
                                    <p>今天也要加油鸭！</p>
                                    <p class="text-muted">4天前，来自提瓦特</p>
                                </div>
                            </li>

                        </ul>
                    </div>

                </div>
            </div>

        </div>



    </div>
    <!-- main content end-->
</section>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
</script>
<script>

    function toChange()
    {
        window.open("/find")
    }
</script>

<script src="js/vue.js"></script>
<script>
    new Vue({
        el:'#app',
        data:{
            msg:'关注',
            my_cls:'btn btn-success btn-sm'
        },
        methods:{
            btnClick:function(){
                if(this.my_cls=='btn btn-success btn-sm'){
                    this.my_cls='btn btn-default btn-sm';
                    this.msg='取消关注';
                    alert("关注成功！");
                }else{
                    this.my_cls='btn btn-success btn-sm';
                    this.msg='关注';
                    alert("取消关注成功！");
                }
            }
        }
    })
</script>
<script>
    new Vue({
        el:'#app1',
        data:{
            msg:'关注',
            my_cls:'btn btn-success btn-sm'
        },
        methods:{
            btnClick:function(){
                if(this.my_cls=='btn btn-success btn-sm'){
                    this.my_cls='btn btn-default btn-sm';
                    this.msg='取消关注';
                    alert("关注成功！");
                }else{
                    this.my_cls='btn btn-success btn-sm';
                    this.msg='关注';
                    alert("取消关注成功！");
                }
            }
        }
    })
</script>
<script src="js/vue.js"></script>

<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>


</body>
</html>
