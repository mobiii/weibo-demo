<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">
    <title>新用户注册界面</title>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" action="registration">
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">新用户注册</h1>
            <img src="../static/images/background.jpg" alt=""/>
        </div>


        <div class="login-wrap">
            <p>输入您的帐户详细信息如下</p>
            <input type="text" autofocus="" placeholder="用户名" class="form-control" name="userName">
            <input type="password" placeholder="密码" class="form-control" name="first_pwd">
            <input type="password" placeholder="确认密码" class="form-control" name="second_pwd">
            <label class="checkbox">
                <input type="checkbox" value="agree this condition"> 我同意服务条款和隐私政策
            </label>
            <div class="registration">
                ${message}
            </div>
            <button type="submit" class="btn btn-lg btn-login btn-block">
                <i class="fa fa-check"></i>
            </button>

            <div class="registration">
                已经注册。
                <a href="login" class="">
                    去登陆
                </a>
            </div>

        </div>

    </form>

</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="../static/js/jquery-1.10.2.min.js"></script>
<script src="../static/js/bootstrap.min.js"></script>
<script src="../static/js/modernizr.min.js"></script>

</body>