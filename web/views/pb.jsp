<%@ taglib prefix="v-on" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>AdminX</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">
    <script src="js/jquery-1.10.2.min.js"></script>
    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
   <script src="js/html5shiv.js"></script>-->
    <!--    <script src="js/respond.min.js"></script>-->
    <![endif]-->
</head>

<body class="sticky-header">
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->
        <div class="panel" id="pInfo">
            <div class="panel-body">
                <div class="media usr-info">
                    <a href="#" class="pull-left">
                        <img class="thumb" :src="mainuser.picture" alt=""/>
                    </a>
                    <div class="media-body" >
                        <h4 class="media-heading"><B>{{mainuser.user_name}}</B></h4>
                        <p>个人简介：{{mainuser.introduction}}</p>
                        <p>{{mainuser.user.blogSum}} 条博客| {{mainuser.user.follows}} 关注| {{mainuser.user.followers}} 位关注者</p>
                        <div  v-on:click="toggle()">
                            <button v-bind:class="mainuser.classes">{{mainuser.message}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11"  style="margin-left: 20px">
                <div class="panel">
                    <header class="panel-heading">
                        最新动态
                        <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                     </span>
                    </header>
                    <div class="panel-body" id="ppp">
                        <ul class="activity-list" v-for="a in items">

                            <br>
                            <div class="avatar">
                                <img :src="a.photo" alt=""/>
                            </div>
                            <div class="activity-desk">
                                <h4><a :href="'personblog?user_id='+a.user_id">{{a.user_name}}</a> </h4>
                                <p href="context"><h4>{{a.article}}</h4>   </p>

                                <div class="album" v-for="(p,id) in a.pic">
                                    <a href="#">
                                        <img :src="p.pic_addr">
                                    </a>
                                </div>

                                <div class="panel">

                                    <div class="panel-body">

                                    </div>
                                    <div class="posttime"></div>

                                    <div>
                                        <ul class="post-view" style="margin-top: 20px">
                                            <li >
                                                <a  :href="'context?user_id='+a.user_id+'&blog_id='+a.blog_id">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <span id="read" > {{a.blog_id}}</span>
                                            </li>
                                            <li >
                                                <a  :href="'context?user_id='+a.user_id+'&blog_id='+a.blog_id">
                                                    <i class="fa fa-comment"></i>
                                                </a>
                                                <span id="comment" >{{a.commentsum}}</span>
                                            </li>
                                            <li v-on:click="changeLike(a)">
                                                <form id="likesform">
                                                    <input type="hidden" name ="user_id" value=${user_id}>
                                                    <input type="hidden" name ="blog_id" :value="a.blog_id">
                                                </form>
                                                <a >
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                                <span id="love" >{{a.likesum}}</span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            </li>

                        </ul>

                    </div>

                </div>
            </div>

        </div>

    </div>
    <!-- main content end-->
</section>

<script>

    function toChange()
    {
        var key1=$("#key1").val();
        window.open("/findPerson?key1="+key1)
    }
</script>


<script src="js/vue.js"></script>
<script>
    var vm = new Vue({
        el:'#ppp',
        data:{
            items:'',
        },
        created:function(){
            var that = this;
            $.ajax({
                url:"/getPersonBlogs?user_id="+${user_id},
                contentType:"application/json;charset=UTF-8",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    that.items = res;
                },error:function (){

                }
            })
        },
        methods: {
            changeLike: function(a) {
                var that = this;
                alert("点赞");
                $.ajax({
                    url:"getPersonBlogs?user_id="+a.user.user_id+"&blog_id="+a.blog_id,
                    contentType:"application/json;charset=UTF-8",
                    dataType:"json",
                    success:function(res){
                        console.log(res);
                        that.mainuser = res;
                    },error:function (){

                    }
                })
            }
        }
    })
</script>
<script>
    var vm = new Vue({
        el:'#pInfo',
        data:{
            mainuser:'',
        },
        created:function(){
            var that = this;
            $.ajax({
                url:"/getPersonalInfo?user_id="+${user_id},
                contentType:"application/json;charset=UTF-8",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    that.mainuser = res;
                },error:function (){

                }
            })
        },
        methods: {
            toggle: function() {
                var that = this;
                $.ajax({
                    url:"/updatePersonalInfo?user_id="+"&status="+that.mainuser.status,
                    contentType:"application/json;charset=UTF-8",
                    dataType:"json",
                    success:function(res){
                        console.log(res);
                        that.mainuser = res;
                    },error:function (){

                    }
                })
            }
        }
    })
</script>
<script src="js/jquery.nicescroll.js"></script>
<script type="text/javascript">
    $(document).ready(
        function() {
            $("html").niceScroll();
        }
    );
</script>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
</script>
<script>
    new Vue({
        el:'#app1',
        data: {
            read:0,
            comment:0,
            love:0
        },
        methods:{
            add:function (){
                this.read=this.read+1;
            },
            addLove:function (){
                this.love=this.love+1;
            }
        }
    })

</script>

<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>


<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>


</body>
</html>
