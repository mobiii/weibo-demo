<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>



<nav>
    <div class="header-section">

        <!--toggle button start-->
        <a class="toggle-btn"><i class="fa fa-bars"></i></a>
        <!--toggle button end-->

        <!--search start-->
        <form class="searchform" action="index" method="post">
            <input type="text" class="form-control" name="keyword" placeholder="搜索账号" value='${key1}' id="key1" onkeydown="javascript:if(event.keyCode==13) toChange()"/>

        </form>
        <!--search end-->

        <!--notification menu start -->
        <div class="menu-right">
            <ul class="notification-menu">
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-head pull-right">
                        <h5 class="title">消息</h5>
                        <ul class="dropdown-list normal-list">
                            <li class="new">
                                <a href="context">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">@我 </span>
                                </a>
                            </li>
                            <li class="new">
                                <a href="">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">评论我  </span>
                                </a>
                            </li>
                            <li class="new">
                                <a href="">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">博客小管家  </span>
                                </a>
                            </li>
                            <li class="new"><a href="notices">查看所有消息</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <input type="hidden" value="${userId}" name="userId" id="userId">
                        <img src=${photo}  />
                        ${name}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        <li><a href="personblog?user"><i class="fa fa-user"></i>  个人博客</a></li>
                        <li><a href="setting"><i class="fa fa-cog"></i>  设置</a></li>
                        <li><a href="login"><i class="fa fa-sign-out"></i> 退出登录</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!--notification menu end -->

    </div>
</nav>


