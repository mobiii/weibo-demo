<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:18
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--之前被坑了很久的，isELIgnored属性如果不加就接收不到参数，可能是版本原因--%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>用户账户管理</title>
    <link rel="stylesheet" type="text/css" href="js/ios-switch/switchery.css" />

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/green.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/blue.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/yellow.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/purple.css" rel="stylesheet">

    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/green.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/yellow.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/purple.css" rel="stylesheet">

    <link href="js/iCheck/skins/flat/grey.css" rel="stylesheet">
    <link href="js/iCheck/skins/flat/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/flat/green.css" rel="stylesheet">
    <link href="js/iCheck/skins/flat/blue.css" rel="stylesheet">
    <link href="js/iCheck/skins/flat/yellow.css" rel="stylesheet">
    <link href="js/iCheck/skins/flat/purple.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="css/clndr.css" rel="stylesheet">
    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!--icheck-->

    <link rel="stylesheet" type="text/css" href="js/ios-switch/switchery.css" />
    <link rel="stylesheet" type="text/css" href="js/jquery-multi-select/css/multi-select.css" />

    <!--multi-select-->
    <!--file upload-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css" />

    <!--tags input-->
    <link rel="stylesheet" type="text/css" href="js/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-datepicker/css/datepicker-custom.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-timepicker/css/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    <link rel="stylesheet" type="text/css" href="js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />
    <!--dashboard calendar-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="sticky-header"  >
<section  style="width: 100%">
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->
        <div class="row" >
            <div class="col-md-12">
                <header class="panel-heading" >
                    <h3>账户信息管理</h3>
                </header>
                <div class="panel-body"  >
                    <form class="form-horizontal" role="form" action="setting" method="post" enctype="multipart/form-data" id="signup-form">

                        <div class="form-group">
                            <label for="username" class="col-lg-2 col-sm-2 control-label">用户昵称</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="username" style="width: 300px" value='${username}' name="userName">
                                <p class="help-block">4～30个字符，支持中英文、数字</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">性别</label>
                            <input type="radio" name="sex" value=${sex} checked>${sex}</input><br/>
                            <input type="radio" name="sex" value=${sex_ab}>${sex_ab}</input>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">选择生日</label>
                            <div class="col-md-4 col-xs-11">

                                <div data-date-viewmode="years" data-date-format="yyyy-mm-dd" data-date="2022-10-20"  class="input-append date dpYears" >
                                    <input type="text"  value='${birthday}' size="16" class="form-control" name="birthday">
                                    <span class="input-group-btn add-on">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                                <span class="help-block">选择生日</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 col-sm-2 control-label">图片上传</label>
                            <div class="col-md-9">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" >
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                                           <span class="btn btn-default btn-file">
                                                           <span class="fileupload-new"><i class="fa fa-paper-clip"></i>选择图片</span>
                                                           <span class="fileupload-exists"><i class="fa fa-undo"></i>更改图片</span>
                                                           <input type="file" class="default" name="photo"/>
                                                           </span>
                                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i>删除图片</a>
                                    </div>
                                    <p class="help-block">上传的图片应小于5M</p>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">邮箱</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control"  id="inputEmail1" value='${email}' name="email" style="width: 300px">
                                <p class="help-block">请输入电子邮箱</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="introduction" class="col-lg-2 col-sm-2 control-label">个人简介</label>
                            <div class="col-lg-9">
                                <textarea id="introduction" class="form-control" placeholder="简单介绍一下你自己" rows="4" name="introduction" style="width: 700px">${introduction}</textarea>
                                <p class="help-block">简单介绍下你自己，支持中英文数字</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">旧密码</label>
                            <div class="col-lg-10">
                                <input type="password" class="form-control" id="inputPassword1" placeholder="输入原密码" style="width: 300px" name="old_pwd">
                                <p class="help-block">4～30个字符，支持中英文、数字</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">新密码</label>
                            <div class="col-lg-10">
                                <input type="password" class="form-control" id="inputPassword2" placeholder="输入新密码" style="width: 300px" name="new pwd">
                                <p class="help-block">4～30个字符，支持中英文、数字</p>
                                <p class="help-block">
                                    ${result}
                                </p>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> 记住我
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit" class="btn btn-primary" onclick="subbmit()">修改</button>
                                <button type="reset" class="btn btn-primary">取消</button>
                            </div>
                        </div>
                        <br><br><br><br><br>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
    let result=$("#result").val();
    function subbmit()
    {
        alert("提交成功，请下拉查看返回结果");
    }
</script>


<script>

    function toChange()
    {
        var key1=$("#key1").val();
        window.open("/findPerson?key1="+key1)
    }
</script>
<script>
    let email= $("#email").val();
    let birthday=$("#birthday").val();
    let introduction=$("#introduction").val();
    let username=$("#username").val();
    let sex=$("#sex").val();
    let ab_sex=$("#ab_sex").val();
</script>
<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--ios7-->
<script src="js/ios-switch/switchery.js" ></script>
<script src="js/ios-switch/ios-init.js" ></script>

<!--multi-select-->
<script type="text/javascript" src="js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>

<!--file upload-->
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="js/pickers-init.js"></script>
<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>
