<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 1:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<nav>
    <div class="left-side sticky-left-side">
    <!--logo and iconic logo start-->
    <div class="logo">
        <a href="index"><img src="images/logo.png" alt=""></a>
    </div>

    <div class="logo-icon text-center">
        <a href="index"><img src="images/logo_icon.png" alt=""></a>
    </div>
    <!--logo and iconic logo end-->

    <div class="left-side-inner">

        <!-- visible to small devices only -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media logged-user">
                <img alt="" src="images/photos/user-avatar.png" class="media-object">
                <div class="media-body">
                    <h4><a href="personblog">John Doe</a></h4>
                    <span>"Hello There..."</span>
                </div>
            </div>

            <h5 class="left-nav-title">Account Information</h5>
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li><a href="personblog?user_id=-1"><i class="fa fa-user"></i> <span>个人博客</span></a></li>
                <li><a href="setting"><i class="fa fa-cog"></i> <span>设置</span></a></li>
                <li><a href="login"><i class="fa fa-sign-out"></i> <span>退出登录</span></a></li>
            </ul>
        </div>

        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav" >
            <li><a href="index">首页</a></li>
            <li><a href="personblog?user_id=-1">个人博客</a></li>
            <li><a href="setting">账户设置</a></li>
        </ul>
        <!--sidebar nav end-->
    </div>
</div>

</nav>
