<%--
  Created by IntelliJ IDEA.
  User: 86158
  Date: 2022/4/24
  Time: 18:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">
    <script src="js/jquery-1.10.2.min.js"></script>
    <title>查找</title>

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/blue.css" rel="stylesheet">

    <!--dashboard calendar-->
    <link href="css/clndr.css" rel="stylesheet">

    <!--common-->
    <link href="css/style.css" rel="stylesheet">
    <script src="js/vue.js"></script>
    <script src="js/jquery-1.10.2.min.js"></script>
    <link href="css/style-responsive.css" rel="stylesheet">
</head>
<body>
<section>
    <!-- left side start-->
    <%@include file="left.jsp"%>
    <!-- left side end-->
    <!-- main content start-->
    <div class="main-content" >
        <!-- header section start-->
        <%@include file="header.jsp"%>
        <!-- header section end-->
        <div class="row" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
               <br>
            </ul>
        </div>
        <div class="row" style="margin-left: 40px">
            <div class="col-md-10" >
                <div class="panel">
                    <div class="panel-body">
                        <div class="dir-info" id="app">
                            <div class="row" v-for="item in items">
                                <div class="col-xs-1">
                                    <div class="avatar">
                                        <img :src="item.picture" alt=""/>
                                    </div>
                                </div>
                                <div class="col-xs-9">
                                    <a href="personBlog"><h4 >{{item.userName}}</h4></a>
                                    <span>个人简介：{{item.introduction}}</span>
                                    </span><br>
                                    <span >{{item.followed}}位粉丝</span>
                                </div>
                                <div  v-on:click="toggle(item)">
                                    <button v-bind:class="item.classes">{{item.message}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
<script>

    var vm = new Vue({
        el:'#app',
        data:{
            items:'',
        },
        created:function(){
            var that = this;
            $.ajax({
                url:"/person",
                contentType:"application/json;charset=UTF-8",
                dataType:"json",
                success:function(res){
                    console.log(res);
                    that.items = res;
                }
            })
        },
        methods: {
            toggle: function(item) {
                var that = this;
                $.ajax({
                    url:"update?user_id="+item.user.user_id+"&status="+item.status,
                    contentType:"application/json;charset=UTF-8",
                    dataType:"json",
                    success:function(res){
                        console.log(res);
                        that.items = res;//覆盖原有内容
                    }
                })
            }
        }
    })

</script>
<script>
    let photo= $("#photo").val();
    let name= $("#name").val();
    let userId=$("#userId").val();
    let key1=$("#key1").val();
</script>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>

<!--common scripts for all pages-->
<script src="js/scripts.js"></script>
<script src="js/vue.js"></script>
</html>