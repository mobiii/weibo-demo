<%--
  Created by IntelliJ IDEA.
  User: 86134
  Date: 2022/4/18
  Time: 0:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="#" type="image/png">

    <title>选择您要提醒的好友</title>

    <!--ios7-->
    <link rel="stylesheet" type="text/css" href="js/ios-switch/switchery.css" />

    <!--icheck-->
    <link href="js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/green.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/blue.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/yellow.css" rel="stylesheet">
    <link href="js/iCheck/skins/minimal/purple.css" rel="stylesheet">

    <link href="js/iCheck/skins/square/square.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/red.css" rel="stylesheet">
    <link href="js/iCheck/skins/square/green.css" rel="stylesheet">
    <link href="js/iCheck/skins/flat/green.css" rel="stylesheet">


    <!--multi-select-->
    <link rel="stylesheet" type="text/css" href="js/jquery-multi-select/css/multi-select.css" />

    <!--file upload-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-fileupload.min.css" />

    <!--tags input-->
    <link rel="stylesheet" type="text/css" href="js/jquery-tags-input/jquery.tagsinput.css" />



    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- main content start-->
    <!--body wrapper start-->
    <div class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        选择您要提醒的好友
                        <span class="tools pull-right"></span>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <form class="form-horizontal bucket-form" method="get">
                                    <div class="form-group">
                                        <div class="col-sm-9 icheck ">

                                            <div class="minimal-green single-row">
                                                <div class="checkbox ">
                                                    <input type="checkbox" >
                                                    <label>麻木不人</label>
                                                </div>
                                            </div>
                                            <div class="minimal-green single-row">
                                                <div class="checkbox ">
                                                    <input type="checkbox" >
                                                    <label>*00</label>
                                                </div>
                                            </div>
                                            <div class="minimal-green single-row">
                                                <div class="checkbox ">
                                                    <input type="checkbox" >
                                                    <label>华小农</label>
                                                </div>
                                            </div>
                                            <div class="minimal-green single-row">
                                                <div class="checkbox ">
                                                    <input type="checkbox" >
                                                    <label>美味的甜甜花酿鸡 </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div style="text-align: center">
                            <button type="submit" class="btn-success" onclick="isRight()">确认</button>
                            <button type="reset" class="btn-default" onclick="notRight()">取消</button>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
    <!--body wrapper end-->
    <!-- main content end-->
</section>
<script>
    function isRight()
    {
        window.close();
    }
    function notRight()
    {
        window.close();
    }
</script>
<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>

<!--ios7-->
<script src="js/ios-switch/switchery.js" ></script>
<script src="js/ios-switch/ios-init.js" ></script>

<!--icheck -->
<script src="js/iCheck/jquery.icheck.js"></script>
<script src="js/icheck-init.js"></script>
<!--multi-select-->
<script type="text/javascript" src="js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script src="js/multi-select-init.js"></script>
<!--spinner-->
<script type="text/javascript" src="js/fuelux/js/spinner.min.js"></script>
<script src="js/spinner-init.js"></script>
<!--file upload-->
<script type="text/javascript" src="js/bootstrap-fileupload.min.js"></script>
<!--tags input-->
<script src="js/jquery-tags-input/jquery.tagsinput.js"></script>
<script src="js/tagsinput-init.js"></script>
<!--bootstrap input mask-->
<script type="text/javascript" src="js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>

