package generate;

import generate.User;

public interface UserDao {
    int deleteByPrimaryKey(Integer blogId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer blogId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}