package generate;

import generate.Picture;

public interface PictureDao {
    int deleteByPrimaryKey(Integer blogId);

    int insert(Picture record);

    int insertSelective(Picture record);

    Picture selectByPrimaryKey(Integer blogId);

    int updateByPrimaryKeySelective(Picture record);

    int updateByPrimaryKey(Picture record);
}