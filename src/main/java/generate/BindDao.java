package generate;

import generate.Bind;

public interface BindDao {
    int deleteByPrimaryKey(Integer bindId);

    int insert(Bind record);

    int insertSelective(Bind record);

    Bind selectByPrimaryKey(Integer bindId);

    int updateByPrimaryKeySelective(Bind record);

    int updateByPrimaryKey(Bind record);
}