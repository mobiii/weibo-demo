package generate;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * blog
 * @author 
 */
@Data
public class Blog implements Serializable {
    private Integer blogId;

    private Integer userId;

    private String article;

    private Date blogTime;

    private static final long serialVersionUID = 1L;
}