package generate;

import generate.Comment;

public interface CommentDao {
    int deleteByPrimaryKey(Integer blogId);

    int insert(Comment record);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Integer blogId);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);
}