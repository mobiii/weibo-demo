package generate;

import generate.Likes;

public interface LikesDao {
    int deleteByPrimaryKey(Integer blogId);

    int insert(Likes record);

    int insertSelective(Likes record);

    Likes selectByPrimaryKey(Integer blogId);

    int updateByPrimaryKeySelective(Likes record);

    int updateByPrimaryKey(Likes record);
}