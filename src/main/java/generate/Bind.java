package generate;

import java.io.Serializable;
import lombok.Data;

/**
 * bind
 * @author 
 */
@Data
public class Bind implements Serializable {
    private Integer bindId;

    private Integer userId;

    private Integer followerId;

    private static final long serialVersionUID = 1L;
}