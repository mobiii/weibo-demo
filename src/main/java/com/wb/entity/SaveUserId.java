package com.wb.entity;

public class SaveUserId {
    //由于切换页面会使得userid无法保存，或者因页面重定向无法用@param获取
    //所以这个类作为一个公共变量类，用来获取保存userID,方便项目内所有代码获取userId
    public static int userId=0;
}
