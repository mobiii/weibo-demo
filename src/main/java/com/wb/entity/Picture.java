package com.wb.entity;
public class Picture {
    private int pic_id;
    private int blog_id;
    private String pic_addr;

    public String toString()
    {
        return  pic_addr;
    }
    public int getPic_id() {
        return pic_id;
    }

    public void setPic_id(int pic_id) {
        this.pic_id = pic_id;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public String getPic_addr() {
        return pic_addr;
    }

    public void setPic_addr(String pic_addr) {
        this.pic_addr = pic_addr;
    }
}
