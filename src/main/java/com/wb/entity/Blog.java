package com.wb.entity;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Blog {
    private int blog_id;
    private int user_id;
    private String article;
    private Timestamp blog_time;
    private List<Comment> comment=new ArrayList<>();
    private List<Likes> likes=new ArrayList<>();
    private List<Picture> pic=new ArrayList<>();
    private  String user_name;
    private String photo;
    private int likesum;
    private int commentsum;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getCommentsum() {
        return commentsum;
    }

    public void setCommentsum(int commentsum) {
        this.commentsum = commentsum;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getLikesum() {
        return likesum;
    }

    public void setLikesum(int likesum) {
        this.likesum = likesum;
    }

    public String toString(){
        return article;
    }
    public List<Likes> getLikes() {
        return likes;
    }

    public void setLikes(List<Likes> likes) {
        this.likes = likes;
    }

    public List<Picture> getPic() {
        return pic;
    }

    public void setPic(List<Picture> pic) {
        this.pic = pic;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }

    public Timestamp getBlog_time() {
        return blog_time;
    }

    public void setBlog_time(Timestamp blog_time) {
        this.blog_time = blog_time;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

}
