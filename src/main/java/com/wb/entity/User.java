package com.wb.entity;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
@Component("user")
public class User {
    //生日还没处理如何导入数据库
    public static int id_Count=8;
    private int user_id;
    private String user_password;
    private char sex;//0代表女，1代表男
    private String user_name;
    private String email;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    private String photo;
    private String introduction;
    private List<Blog> blogs =new ArrayList<>();
    private List<Picture> pictures = new ArrayList<>();
    private int blogSum;
    private  int follows;
    private  int followers;

    public int getFollows() {
        return follows;
    }

    public void setFollows(int follows) {
        this.follows = follows;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public static void setId_Count(int id_Count) {
        User.id_Count = id_Count;
    }

    public int getBlogSum() {
        return blogSum;
    }

    public void setBlogSum(int blogSum) {
        this.blogSum = blogSum;
    }

    public User()//这里在Java内部生成主码id，不用Java自带的无参构造器
    {
        id_Count++;
        this.user_id=id_Count;
    }

    public static int getId_Count() {

        return id_Count;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String intrduction) {
        this.introduction = intrduction;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }



    public int getUser_id() {
        return user_id;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
