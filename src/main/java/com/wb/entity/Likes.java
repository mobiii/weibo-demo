package com.wb.entity;
public class Likes {
    private int likes_id;
    private int blog_id;
    private int user_id;
    public static int id_Count=8;
    public Likes()
    {
        id_Count++;
        this.likes_id=id_Count;
    }
    public String toString(){
        return "likes_id:"+this.user_id;
    }
    public int getLikes_id() {
        return likes_id;
    }

    public void setLikes_id(int likes_id) {
        this.likes_id = likes_id;
    }

    public int getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(int blog_id) {
        this.blog_id = blog_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
