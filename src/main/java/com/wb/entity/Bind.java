package com.wb.entity;

public class Bind {
    private static int count_id=15;
    private int bind_id;
    private int user_id;
    private int follower_id;
    public Bind()
    {
        count_id++;
        this.bind_id=count_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getBind_id() {
        return bind_id;
    }

    public void setBind_id(int bind_id) {
        this.bind_id = bind_id;
    }

    public int getFollower_id() {
        return follower_id;
    }

    public void setFollower_id(int follower_id) {
        this.follower_id = follower_id;
    }
}
