package com.wb.dao;

import com.wb.entity.Blog;
import com.wb.entity.Picture;
import com.wb.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;

public interface UserMapper {
    void addUser(User user);
    User getUserById(int id);
    void deleteUser(int id);
    //一个submit 接受之后主动传成user过来就好
    void updateUser(User user);
    //获得某人的所有博客
    User findOnesBlogs(User u );
    //获得某人的所有图片
    User findOnesPic(User u );
    //通过一个人的userid获取所有关注的对象 ResultMap
    List<User> findAllFollows(User user);
    //通过一个人的userid获取所有粉丝
    List<User> findAllFollowers(User user);

    List<Blog> getOnesBlogs(User u);

    List<Picture> getOnesPic(User u);

    Integer getBlogSum(User u);

    Integer getFollowSum(User u);

    Integer getFollowerSum(User u);
}
