package com.wb.dao;

import com.wb.entity.Likes;

public interface LikesMapper {
    //点赞
    void addLikes(Likes likes);
    //取消
    void deleteLikes(Likes likes);
}
