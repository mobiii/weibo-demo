package com.wb.dao;

import com.wb.entity.Blog;
import com.wb.entity.Comment;
import com.wb.entity.Picture;
import com.wb.entity.User;

import java.util.List;

public interface BlogMapper {
    //寻找博客
    Blog findBlogById(int i);
    //删除博客
    void deleteBlog(Blog blog);
    //增加博客
    void addBlog(Blog blog);
    //通过Uid找到所有博客
    List<Blog> getBlogs(Blog blog);//传过来一个blog对象 找到发这条blog的人的所有blog
    //获取一条博客下所有图片
    Blog getAllPic(Blog blog);
    //获取某条博客的点赞数
    Integer getLikesSum(Blog blog);
    //获取一条博客下所有点赞的user_id
    Blog getAllLikes(Blog blog);
    //获取一条博客下所有评论
    Blog getAllComment(Blog blog);
    //获取首页所有博客
    List<Blog> getAllBlogs();

    List<Comment> getCommentList(Blog blog);

    List<Picture> getPicList(Blog blog);

    Integer getCommentSum(Blog blog);

    Integer getCurrentBID();

    List<User> getLikesUser(Blog blog);

}
