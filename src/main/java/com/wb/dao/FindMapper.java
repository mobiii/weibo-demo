package com.wb.dao;

import com.wb.entity.Blog;
import com.wb.entity.User;

import java.util.List;

public interface FindMapper {
    List<User> selectLikeUser(String name) ;
    List<Blog> selectLikeBlog(String word);
}
