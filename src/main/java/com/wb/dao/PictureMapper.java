package com.wb.dao;

import com.wb.entity.Picture;

//对于一个图片而言不存在修改
public interface PictureMapper {
    //增加图片
    void addPicture(Picture picture);
    //删除图片
    void deletePicture(Picture picture);
}
