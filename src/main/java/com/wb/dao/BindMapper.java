package com.wb.dao;

import com.wb.entity.Bind;

public interface BindMapper {
    //增加关注
    void addBind(Bind bind);
    //删除关注
    void deleteBind(Bind bind);
}
