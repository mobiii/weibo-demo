package com.wb.dao;

import com.wb.entity.Comment;

public interface CommentMapper {
    //增加评论
    void addComment(Comment comment);
    //删除评论
    void deleteComment(Comment comment);

}
