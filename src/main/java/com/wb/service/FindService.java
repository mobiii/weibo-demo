package com.wb.service;

import com.wb.dao.FindMapper;
import com.wb.dao.UserMapper;
import com.wb.entity.Blog;
import com.wb.entity.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class FindService {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
    UserMapper user_Mapper=(UserMapper) applicationContext.getBean("userMapper");
    FindMapper find_Mapper=(FindMapper) applicationContext.getBean("findMapper");
    public List<User> findPerson(String s)
    {
        String name="%"+s+"%";
        System.out.println("被调用了"+name);
        List<User> result_Users = find_Mapper.selectLikeUser(name);
        System.out.println("返回结果为"+result_Users.size());
        for(User u: result_Users){
            System.out.println("#######用户="+u.getUser_name());
        }
        return result_Users;

    }
    public List<Blog> findBlog(String s)
    {
        String name="%"+s+"%";
        System.out.println("被调用了"+name);
        List<Blog> result_Blogs = find_Mapper.selectLikeBlog(name);
        System.out.println("返回结果为"+result_Blogs.size());
        for(Blog u: result_Blogs){
            System.out.println("#######用户="+u.getArticle());
        }
        return result_Blogs;

    }
/*测试类
    public static void main(String[] args) {
        FindService findService=new FindService();
        System.out.println("开始查找");
        findService.findBlog("n");
    }*/
}
