package com.wb.service;

import com.wb.dao.UserMapper;
import com.wb.entity.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.codehaus.jackson.PrettyPrinter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;

import java.time.format.DateTimeFormatter;
import java.util.Date;


/***
 * @author *00
 * 用来接受controller下放的一些服务
 * userLogin        ==>Login        ==>登录界面
 * userNew          ==>registration ==>注册界面
 * userSetting      ==>setting      ==>设置界面(更改账密信息)
 * userMessage      ==>setting      ==>获取用户原有值，比如个性签名什么
 */
public class UserService {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
    UserMapper user_mapper=(UserMapper) applicationContext.getBean("userMapper");

    //将前台传过来的用户账密与后台存储的账密对比，判定是否登录成功
    //暂未连接数据库
    public String userLogin(int userId,String pass) {//连接数据库
        User user=(User) user_mapper.getUserById(userId);
        //数据库查不到这个id
        if(user==null)
        {
            System.out.println("用户名不存在");
            return "userId isn't exits";
        }
        //如果用户输入的密码与数据库密码不一致
        else if(!user.getUser_password().equals(pass))
        {
            System.out.println("密码错误");
            return "password is wrong";
        }
        else
        {
            System.out.print("success! ");
            System.out.println(userId+"的用户名为："+user.getUser_name());
            return "Login successfully";//登录成功
        }
    }
    //用于注册用户后与后端连接的service方法
    public int userNew(String username,String pass)
    {
        //此处应该补充将内容写入数据库的代码
        User user = new User();
        user.setUser_name(username);
        user.setUser_password(pass);
        user_mapper.addUser(user);
        //我希望导入数据库会返回id
        return User.getId_Count();
    }
    //用于注册用户之后，用户主动更改自己信息的service方法
    public String userSetting(User user,String old_pwd)
    {
        //数据库根据id返回数据库储存的密码
        System.out.println("更新代码被执行！");
        User return_user=(User)user_mapper.getUserById(user.getUser_id());
        String return_pass;//要对用户进行比较的标准密码
        if(return_user.getUser_password()!=null)
        {
            return_pass=return_user.getUser_password();
            if(!old_pwd.equals(return_pass))
            {
                return "old password is wrong";
            }
            else//密码无误，进入更新环节
            {
//                return_user.setSex('f');
                System.out.println("我是更新代码，主人你好，我收到的数据是："+user.getPhoto()+user.getUser_name());
                user_mapper.updateUser(user);
                return "success";
            }
        }
        else
            return "userId isn't exits";
        //如果旧密码错误
    }
    public User userMessage(int userId)
    {
        User user=(User)user_mapper.getUserById(userId);
        return user;

    }

}
