package com.wb.service;

import com.wb.dao.*;
import com.wb.entity.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/***
 * @author *00
 * 用来接受controller下放的一些服务
 * addComment             ==>(String)success        ==>新增评论
 * addLike/deleteLike     ==>(int)like_sum          ==>点赞哥
 * addBind/deleteBind     ==>(int)                  ==>被关注人数
 * getAttention           ==>(int)                  ==>被关注人数
 * getBlog/getBlogNum     ==>Blog/(int)             ==>查找博客/博客总数
 */
public class BlogService {
    ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
    UserMapper user_Mapper=(UserMapper) applicationContext.getBean("userMapper");
    BlogMapper blog_Mapper=(BlogMapper) applicationContext.getBean("blogMapper");
    LikesMapper like_Mapper=(LikesMapper) applicationContext.getBean("likesMapper");
    PictureMapper picture_Mapper=(PictureMapper)applicationContext.getBean("pictureMapper");
    CommentMapper comment_Mapper=(CommentMapper)applicationContext.getBean("commentMapper");
    BindMapper bind_Mapper=(BindMapper)applicationContext.getBean("bindMapper");

    //添加评论
    public String addComment(Comment comment)
    {
        comment_Mapper.addComment(comment);
        System.out.println("更新成功！");
        return "success";
    }

    //点赞
    public int addLike(Likes likes)
    {
        like_Mapper.addLikes(likes);
        return blog_Mapper.getLikesSum(blog_Mapper.findBlogById(likes.getBlog_id()));//返回一共有多少人点赞
    }
    //取消点赞
    public int deleteLike(Likes likes)
    {
        like_Mapper.deleteLikes(likes);
        return blog_Mapper.getLikesSum(blog_Mapper.findBlogById(likes.getBlog_id()));//返回一共有多少人点赞
    }
    public int addBind(Bind bind)
    {//返回被关注的人数
        bind_Mapper.addBind(bind);
        //根据被关注人的id获取被关注人的信息
        User user=user_Mapper.getUserById(bind.getUser_id());
        return user_Mapper.findAllFollowers(user).size();
    }
    public int deleteBind(Bind bind)
    {//返回被关注的人数
        bind_Mapper.deleteBind(bind);
        User user=user_Mapper.getUserById(bind.getUser_id());
        return user_Mapper.findAllFollowers(user).size();
    }
    public List<User> getFollowed(User user)
    {//返回该用户关注的人
        List<User> list=user_Mapper.findAllFollowers(user);
        return list;
    }
    public List<User> getFollowing(User user)
    {
        List<User> list=user_Mapper.findAllFollows(user);
        return list;
    }
    public int getBlogNum(User user)
    {//返回博客总数
        int result=user_Mapper.findOnesBlogs(user).getBlogs().size();
        return result;
    }
    public Blog getBlog(int blogId)
    {//获取某一博客
        Blog blog=blog_Mapper.findBlogById(blogId);
        return blog;
    }
    /*测试类
    //要返回博客阅读总数直接获取blog然后调取blog的seen就好！！！
    public static void main(String[] args) {
//        Comment c=new Comment();
//        c.setBlog_id(3);
//        c.setUser_id(1);
//        c.setComment_id(4);
//        c.setMessage("today is rainy");
        BlogService blogService = new BlogService();
        UserService userService = new UserService();
//        blogService.addComment(c);
        点赞测试
        Likes likes=new Likes();
        likes.setBlog_id(1);
        likes.setLikes_id(5);
        likes.setUser_id(1);
        blogService.deleteLike(likes);
//被关注的人数
       /* Bind bind = new Bind();
        bind.setFollower_id(1);//关注者的id
        bind.setUser_id(3);//被关注者的id
//        blogService.addBind(bind);
        System.out.println(bind.getBind_id());
//        blogService.deleteBind(bind);
        User user = userService.userMessage(bind.getUser_id());//被关注人的user对象
        System.out.println(bind.getUser_id() + "受关注度为" + blogService.deleteBind(bind));
        获取博客
        User user=userService.userMessage(1);
        System.out.println(blogService.getBlogNum(user));
        Blog blog= blogService.getBlog(1);
        System.out.println(blog.getArticle()+"是由用户"+blog.getUser_id()+"写的"+ blog.getSeen());


    }

    }*/
}
