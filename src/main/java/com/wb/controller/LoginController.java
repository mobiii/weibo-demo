package com.wb.controller;

import com.wb.entity.SaveUserId;
import com.wb.entity.User;
import com.wb.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
@Controller

public class LoginController {
    //用户首先进入的界面，从这里获取唯一的userid，以便后续界面从数据库获取数据
    @RequestMapping(value="/login")
    public ModelAndView login(@RequestParam(value = "userId",required = false)String userId,
                              @RequestParam(value = "password",required = false)String password) {
        ModelAndView mdv= new ModelAndView();
        UserService uservice =new UserService();
        System.out.println("开始启动"+userId+password);
        //前端获取值获取不了int类型，只能先获取string再进行转化
        if(userId==null) {
            mdv.setViewName("login");
            return mdv;
        }
        if(userId!=null && password!=null){
            int userid_int= Integer.parseInt(userId);
            String result= uservice.userLogin(userid_int,password);
            System.out.println(userId + password + "调用成功");
            if(result.equals("Login successfully"))
            {//登录成功，返回userId，便于其他类使用
                mdv.setViewName("index");

                mdv.addObject("message","登录成功！");
                //借助一个静态int类保存userID
                SaveUserId.userId=userid_int;
                System.out.println("当前登录账户为："+userId);
                return mdv;
            }
            else if(result.equals("userId isn't exits")) {
                mdv.setViewName("login");
                mdv.addObject("message","账户不存在！");
                return mdv;
            }
            else {
                mdv.setViewName("login");
                mdv.addObject("message","密码错误！");
                return mdv;
            }
        }
        //原始界面
        else
        {
            mdv.addObject("message","请输入账号和密码！");
            mdv.setViewName("login");
        }
        return mdv;
    }
}
