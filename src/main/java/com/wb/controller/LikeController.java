package com.wb.controller;

import com.wb.entity.SaveUserId;
import com.wb.entity.User;
import com.wb.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LikeController {
    //notices是通知用户的内容
    @RequestMapping(value="/notices")
    public ModelAndView notices(ModelMap model) {
        ModelAndView mdv  = new ModelAndView();
        UserService uservice=new UserService();
        //准备用户名称和头像
        if(SaveUserId.userId==0) {
            mdv.addObject("photo", "/picture/user1.png");
            mdv.addObject("name", "匿名");
            mdv.setViewName("notices");
            return mdv;
        }
        else {
            User user = uservice.userMessage(SaveUserId.userId);
            mdv.addObject("photo", user.getPhoto());
            mdv.addObject("name", user.getUser_name());
        }
        return mdv;

    }

}
