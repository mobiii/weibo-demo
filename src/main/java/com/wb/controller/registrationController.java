package com.wb.controller;

import com.wb.entity.User;
import com.wb.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
@Controller

public class registrationController {
    //registration是注册界面
    @RequestMapping(value="/registration")
    public ModelAndView registration(@RequestParam(value = "userName",required = false)String userName,
                                     @RequestParam(value = "first_pwd",required = false)String first_pwd,
                                     @RequestParam(value = "second_pwd",required = false)String second_pwd,
                                     @RequestParam(value = "userId",required = false)String userId) {
        ModelAndView mdv = new ModelAndView();
        UserService uservice = new UserService();
        System.out.println(userName+" "+first_pwd+" "+second_pwd);

        //强制用户输入三个信息
        if(userName==null || first_pwd==null || second_pwd==null)
        {
            mdv.addObject("message","输入不能为空");
            mdv.setViewName("registration");
        }
        //两次密码输入不一致
        else if(!first_pwd.equals(second_pwd))
        {
            mdv.addObject("message","两次密码输入不一致");
            mdv.setViewName("registration");
        }
        else
        {
            int result=uservice.userNew(userName,first_pwd);
            if(result!=0)//用户id不为0，默认int不为null
            {
                int userid_int= Integer.parseInt(userId);
                userId=Integer.toString(result);
                mdv.addObject("message","创建成功!您的用户id为"+result+"。请登录");
                mdv.addObject("userId",userId);
                mdv.setViewName("login");
            }
            else
            {
                mdv.addObject("message","创建失败");
                mdv.setViewName("registration");
            }
        }

        return mdv;
    }
}
