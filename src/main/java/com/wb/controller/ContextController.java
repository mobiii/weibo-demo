package com.wb.controller;

import com.wb.dao.*;
import com.wb.entity.*;
import com.wb.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
public class ContextController {
    //context是一条博客的单独页面，主要用于评论点赞
    @RequestMapping(value="/context")
    public String Context(ModelMap model, @RequestParam(value = "user_id",required = false)int user_id,@RequestParam(value = "blog_id",required = false)int blog_id) {
        model.addAttribute("user_id", SaveUserId.userId);
        model.addAttribute("blog_id", blog_id);
        return "context";
    }
    @RequestMapping(value="/getContext")
    @ResponseBody
    public Blog getContext(ModelMap model,@RequestParam(value = "blog_id",required = false)int blog_id){
        ModelAndView mdv  = new ModelAndView();
        UserService uservice=new UserService();
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
        BlogMapper blogMapper = (BlogMapper) applicationContext.getBean("blogMapper");
        UserMapper userMapper = (UserMapper) applicationContext.getBean("userMapper");
        Blog blog = blogMapper.findBlogById(blog_id);
        int user_id=blog.getUser_id();
        User user = userMapper.getUserById(user_id);
        blog.setUser_name(user.getUser_name());
        //获得头像
        if(user.getPhoto()==null){
            blog.setPhoto("picture/波斯猫.jpg");
        }else {
            blog.setPhoto(user.getPhoto());
        }
        int b=blogMapper.getLikesSum(blog);
        blog.setLikesum(b);
        int d=blogMapper.getCommentSum(blog);
        blog.setCommentsum(d);
        List<Comment> c = blogMapper.getCommentList(blog);
        blog.setComment(c);
        List<Picture> p = blogMapper.getPicList(blog);
        blog.setPic(p);
        //准备用户名称和头像
        /*if(SaveUserId.userId==0) {
            mdv.addObject("photo", "/picture/user1.png");
            mdv.addObject("name", "匿名");
            mdv.setViewName("context");
            mdv.addObject("blog", blog);
            return mdv;
        }
        else {
            User user1 = uservice.userMessage(SaveUserId.userId);
            mdv.addObject("user_id",user_id);
            mdv.addObject("photo", "/picture/"+user.getPhoto());
            mdv.addObject("name", user.getUser_name());

        }*/
        return blog;
    }
    @RequestMapping("/addCmt")
    public void addCmt(@RequestParam(value = "blog_id",required = false) String blog_id,
                       @RequestParam(value = "comment",required = false)String message,
                       @RequestParam(value = "user_id")String user_id){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
        CommentMapper commentMapper = (CommentMapper) applicationContext.getBean("commentMapper");
        Comment comment = new Comment();
        int uid = Integer.valueOf(user_id);
        int bid = Integer.valueOf(blog_id);
        comment.setBlog_id(bid);
        comment.setMessage(message);
        comment.setUser_id(uid);
        commentMapper.addComment(comment);
    }
    @ResponseBody
    @RequestMapping("/addLike")
    public void addLike(@RequestParam(value = "blog_id",required = false) String blog_id,
                        @RequestParam(value = "user_id")String user_id){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
        LikesMapper likesMapper = (LikesMapper) applicationContext.getBean("likesMapper");
        BlogMapper blogMapper = (BlogMapper) applicationContext.getBean("blogMapper");
        Likes likes = new Likes();
        int uid = Integer.valueOf(user_id);
        int bid = Integer.valueOf(blog_id);
        likes.setBlog_id(bid);
        likes.setUser_id(uid);
        likesMapper.addLikes(likes);
        Blog blog = blogMapper.findBlogById(bid);
        int likesum=blogMapper.getLikesSum(blog);
    }
    @RequestMapping(value="/getCmt")
    @ResponseBody
    public List<Comment> getCmt(@RequestParam(value = "blog_id")int blog_id){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
        BlogMapper blogMapper = (BlogMapper) applicationContext.getBean("blogMapper");
        UserMapper userMapper = (UserMapper) applicationContext.getBean("userMapper");
        Blog blog  = blogMapper.findBlogById(blog_id);
        List<Comment> list = blogMapper.getCommentList(blog);
        for (Comment comment:list) {
            int a = comment.getUser_id();
            User user=userMapper.getUserById(a);
            comment.setUser_name(user.getUser_name());
            //获得头像
            if(user.getPhoto()==null){
                comment.setPhoto("picture/波斯猫.jpg");
            }else {
                comment.setPhoto(user.getPhoto());
            }
        }
        return list;}


}
