package com.wb.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wb.dao.BlogMapper;
import com.wb.entity.SaveUserId;
import com.wb.entity.User;
import com.wb.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/***主页控制器
 * @author *00
 * @since 2022年4月8日
 */

@Controller

public class MainController {

    int userId=0;//用户账号，主键
    UserService uservice = new UserService();
    @RequestMapping(value="/")
    public String root(ModelMap model) {
        return "redirect:index";
    }

    /**分割线**/
    /**************************我是一条优雅的分割线*****************************/



    //littlepage是另外写一个jsp网页，方便用户勾选想要@的人
    @RequestMapping(value="/littlePage")
    public String littlePage(ModelMap model) {
        model.addAttribute("message", "Spring 3 MVC Hello World");
        return "littlePage";
    }
    //location是另外写的一个jsp网页，用于和百度api对接，以获得当前所在城市
    @RequestMapping(value="/location")
    public String location(ModelMap model) {
        model.addAttribute("message", "Spring 3 MVC Hello World");
        return "location";
    }
    //调试专用result.jsp,比如没有成功过的新技术等东东
    @RequestMapping(value="/result")
    public ModelAndView result() {
        ModelAndView mdv = new ModelAndView();

        mdv.setViewName("result");
        return mdv;
    }
    @RequestMapping(value="/404")
    public String notFound(ModelMap model) {
        model.addAttribute("message", "Spring 3 MVC Hello World");
        return "404";
    }
    @RequestMapping(value="/test")
    public String test(ModelMap model) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-dao.xml");
        BlogMapper blogMapper = (BlogMapper) applicationContext.getBean("blogMapper");
        List list =blogMapper.getAllBlogs();
        String str = objectMapper.writeValueAsString(list);
        model.addAttribute("list",list);
        model.addAttribute("jsontest",str);
        model.addAttribute("message", "Spring 3 MVC Hello World");
        return "test";
    }
    @RequestMapping(value="/500")
    public String errorPage(ModelMap model) {
        model.addAttribute("message", "Spring 3 MVC Hello World");
        return "500";
    }
    @RequestMapping(value="/index")
    public ModelAndView abc(ModelMap model) {

        ModelAndView mv=new ModelAndView();
        //准备用户名称和头像
        if(SaveUserId.userId==0) {
            mv.addObject("photo", "/picture/user1.png");
            mv.addObject("name", "匿名");
            mv.addObject("userId",SaveUserId.userId);
        }
        else {
            User user = uservice.userMessage(SaveUserId.userId);
            mv.addObject("photo", user.getPhoto());
            mv.addObject("name", user.getUser_name());
            mv.addObject("userId",SaveUserId.userId);

        }
        mv.setViewName("abc");
        return mv;
    }
    @RequestMapping(value="/try")
    public String handletest(ModelMap model) {
        model.addAttribute("message", "Spring 3 MVC Hello World");
        return "try";
    }
}
