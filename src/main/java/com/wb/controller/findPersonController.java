package com.wb.controller;

import com.wb.entity.Bind;
import com.wb.entity.SaveUserId;
import com.wb.entity.SimpleUser;
import com.wb.entity.User;
import com.wb.service.BlogService;
import com.wb.service.FindService;
import com.wb.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class findPersonController {
    List<SimpleUser> return_List;//为了提高效率，共享这部分数据
    String key;
    @RequestMapping(value="/findPerson")
    public ModelAndView findPerson(@RequestParam(value = "key1",required = false)String key1) {
        ModelAndView mdv = new ModelAndView();
        this.key=key1;//方便下文查找
        mdv.setViewName("findPerson");
        UserService uservice=new UserService();
        //准备用户名称和头像
        if(SaveUserId.userId==0) {
            mdv.addObject("photo", "/picture/user1.png");
            mdv.addObject("name", "匿名");
        }
        else {
            User user = uservice.userMessage(SaveUserId.userId);
            mdv.addObject("photo", user.getPhoto());
            mdv.addObject("name", user.getUser_name());
        }
        mdv.addObject("key1",key1);
        System.out.println("您要查找的关键字为:"+key1);

        return mdv;
    }
    @RequestMapping(value="/person")
    @ResponseBody
    public List<SimpleUser> getMessage() {
        FindService findService=new FindService();
        BlogService blogService=new BlogService();
        UserService userService=new UserService();
        List<User> list=findService.findPerson(key);
        return_List=new ArrayList<>();
        User user = userService.userMessage(SaveUserId.userId);//获取当前登录账户
        List<User> bindUser=blogService.getFollowing(user);//获取当前账户主动关注的人
        HashMap<Integer,String> findMap=new HashMap<>();
        for(int i = 0; i < bindUser.size(); i++)//用HashMap存储方便进行查找的数据结构
        {
            User u=bindUser.get(i);
            findMap.put(u.getUser_id(),u.getUser_name());
        }
        for (int i = 0; i < list.size(); i++) {
            SimpleUser simpleUser=new SimpleUser();
            simpleUser.setUser(list.get(i));
            if(!findMap.containsKey(simpleUser.getUser().getUser_id())) {//当前用户没有关注对应的用户
                simpleUser.setStatus(true);
                simpleUser.setMessage("关注");
                simpleUser.setClasses("btn btn-success btn-sm");
            }
            else
            {
                simpleUser.setStatus(false);
                simpleUser.setMessage("已关注");
                simpleUser.setClasses("btn btn-default btn-sm");
            }
            simpleUser.setMethod("personBlog?user_id="+simpleUser.getUser().getUser_id());//跳转连接
            simpleUser.setIntroduction(list.get(i).getIntroduction());//自我介绍
            simpleUser.setPicture(list.get(i).getPhoto());//头像
            simpleUser.setFollowed(blogService.getFollowed(list.get(i)).size());//粉丝数
            simpleUser.setUserName(list.get(i).getUser_name());//用户名
            return_List.add(simpleUser);
        }
        return return_List;
    }
    @RequestMapping(value="/update")
    @ResponseBody
    public List<SimpleUser> change(@RequestParam(value = "user_id",required = false)String user_id,
                             @RequestParam(value = "status",required = false)String status)
    {
        if(return_List==null) return this.getMessage();//未被初始化是不可能被调用的
        SimpleUser simpleUser=new SimpleUser();
        UserService userService=new UserService();
        BlogService blogService=new BlogService();
        int userId=Integer.parseInt(user_id);
        System.out.println("我又被摁到啦"+userId+status);
        User user=userService.userMessage(userId);

//从"/person"获得的东西，只有是否关注、按钮样式、粉丝数需要更改
        for(int i=0;i<return_List.size();i++)
        {
            simpleUser=return_List.get(i);
            if(simpleUser.getUser().getUser_id()==userId)
            {//当前对象是用户进行bind操作的对象
                Bind bind=new Bind();
                bind.setUser_id(userId);
                bind.setFollower_id(SaveUserId.userId);

                if(status.equals("true"))//true代表原来是绿色（还没关注要关注）
                {
                    simpleUser.setStatus(false);
                    simpleUser.setMessage("已关注");
                    simpleUser.setClasses("btn btn-default btn-sm");
                    simpleUser.setFollowed(blogService.addBind(bind));
                }
                else//（已经关注要取消关注）
                {
                    //取消关注
                    simpleUser.setFollowed(blogService.deleteBind(bind));
                    simpleUser.setStatus(true);
                    simpleUser.setMessage("关注");
                    simpleUser.setClasses("btn btn-success btn-sm");
                }

                System.out.println(simpleUser.getUserName()+simpleUser.getPicture()+
                        simpleUser.isStatus()+simpleUser.getClasses());

            }
        }

        System.out.println("我回掉了=================================");

        return return_List;
    }
}
